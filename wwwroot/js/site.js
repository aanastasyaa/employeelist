﻿$(document).ready(function () {
    $('#employeeTable thead tr:eq(1) th').each(function (i) {
        var title = $(this).text();
        if (title !== "") {
            $(this).html('<input type="text" placeholder="' + title + '" class="column_search" />');
            $('input', this).on('keyup change', function () {
                if (table.column(i).search() !== this.value) {
                    table
                        .column(i)
                        .search(this.value)
                        .draw();
                }
            });
        }
    });
    var table = $('#employeeTable').DataTable({
        "searching": true,
        "columnDefs": [
            { "orderable": false, "targets": [8] }
        ],
        "lengthChange": false,
        "scrollX": true,
        "orderCellsTop": true,
        "fixedHeader": true,
        "autoWidth": false
    });

    $(".employeeDelete_btn").on('click', function (e) {
        return window.confirm("Вы уверены, что хотите удалить этого сотрудника?");
    });
    $(".positionDelete-btn").on('click', function (e) {
        return window.confirm("Вы уверены, что хотите удалить эту должность?");
    });
    $(".organizationDelete-btn").on('click', function (e) {
        return window.confirm("Вы уверены, что хотите удалить эту организацию?");
    });

    $(".editItem-btn").on('click', function (e) {
        e.preventDefault();
        $(this).closest(".item-row").find("input[type=text]").prop('readonly', false);
        $(this).closest(".item-row").find(".editFormBtns").show();
        $(this).closest(".col-4").hide();
    });
    $(".itemEditCancel-btn").on('click', function () {
        $(this).parent().siblings("input[type=text]").prop('readonly', true);
        $(this).parent().hide();
        $(this).closest(".item-row").children(".col-4").show();
    });
    $(".itemPostEdit-btn").on('click', function () {
        $(this).parent().siblings("input[type=text]").prop('readonly', true);
        $(this).parent().hide();
        $(this).closest(".item-row").children(".col-4").show();
    });
    $(".editFormBtns").hide();

    //Position Modal
    $("#modal-container").on('show.bs.modal', function (event) {
        var triggerControl = $(event.relatedTarget); 
        var url = triggerControl.attr("href");
        var modal = $(this);

        modal.find('.modal-content').load(url);
    });
    $('#modal-container').on('hidden.bs.modal', function () {

        $(this).removeData('bs.modal');
        $('#modal-container .modal-content').empty();
    });

    $("#modal-container").on('submit', function (e) {
        e.preventDefault();
        var form = $(this).find('form');
        var actionUrl = form.attr('action');
        var dataToSend = form.serialize();

        $.post(actionUrl, dataToSend).done(function (data) {
            $('#modal-container .modal-content').empty();
            $('#modal-container .modal-content').append(data);
            var isValid = ($(data).find('input[name="IsValid"]').val() === "True");
            if (isValid) {
                $("#modal-container").modal('hide');
                window.location.reload();
            }
        });
    });

});

