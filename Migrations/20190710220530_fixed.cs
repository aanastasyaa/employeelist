﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EmployeeTable.Migrations
{
    public partial class @fixed : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Employees_Organizations_PositionID",
                table: "Employees");

            migrationBuilder.DropForeignKey(
                name: "FK_Employees_Workplace_WorkplaceID",
                table: "Employees");

            migrationBuilder.DropForeignKey(
                name: "FK_Workplace_Departments_DepartmentID",
                table: "Workplace");

            migrationBuilder.DropForeignKey(
                name: "FK_Workplace_Organizations_OrganizationID",
                table: "Workplace");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Workplace",
                table: "Workplace");

            migrationBuilder.RenameTable(
                name: "Workplace",
                newName: "Workplaces");

            migrationBuilder.RenameIndex(
                name: "IX_Workplace_DepartmentID_OrganizationID",
                table: "Workplaces",
                newName: "IX_Workplaces_DepartmentID_OrganizationID");

            migrationBuilder.RenameIndex(
                name: "IX_Workplace_OrganizationID",
                table: "Workplaces",
                newName: "IX_Workplaces_OrganizationID");

            migrationBuilder.AlterColumn<int>(
                name: "PositionID",
                table: "Employees",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Workplaces",
                table: "Workplaces",
                column: "ID");

            migrationBuilder.AddForeignKey(
                name: "FK_Employees_Organizations_PositionID",
                table: "Employees",
                column: "PositionID",
                principalTable: "Organizations",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Employees_Workplaces_WorkplaceID",
                table: "Employees",
                column: "WorkplaceID",
                principalTable: "Workplaces",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Workplaces_Departments_DepartmentID",
                table: "Workplaces",
                column: "DepartmentID",
                principalTable: "Departments",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Workplaces_Organizations_OrganizationID",
                table: "Workplaces",
                column: "OrganizationID",
                principalTable: "Organizations",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Employees_Organizations_PositionID",
                table: "Employees");

            migrationBuilder.DropForeignKey(
                name: "FK_Employees_Workplaces_WorkplaceID",
                table: "Employees");

            migrationBuilder.DropForeignKey(
                name: "FK_Workplaces_Departments_DepartmentID",
                table: "Workplaces");

            migrationBuilder.DropForeignKey(
                name: "FK_Workplaces_Organizations_OrganizationID",
                table: "Workplaces");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Workplaces",
                table: "Workplaces");

            migrationBuilder.RenameTable(
                name: "Workplaces",
                newName: "Workplace");

            migrationBuilder.RenameIndex(
                name: "IX_Workplaces_DepartmentID_OrganizationID",
                table: "Workplace",
                newName: "IX_Workplace_DepartmentID_OrganizationID");

            migrationBuilder.RenameIndex(
                name: "IX_Workplaces_OrganizationID",
                table: "Workplace",
                newName: "IX_Workplace_OrganizationID");

            migrationBuilder.AlterColumn<int>(
                name: "PositionID",
                table: "Employees",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddPrimaryKey(
                name: "PK_Workplace",
                table: "Workplace",
                column: "ID");

            migrationBuilder.AddForeignKey(
                name: "FK_Employees_Organizations_PositionID",
                table: "Employees",
                column: "PositionID",
                principalTable: "Organizations",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Employees_Workplace_WorkplaceID",
                table: "Employees",
                column: "WorkplaceID",
                principalTable: "Workplace",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Workplace_Departments_DepartmentID",
                table: "Workplace",
                column: "DepartmentID",
                principalTable: "Departments",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Workplace_Organizations_OrganizationID",
                table: "Workplace",
                column: "OrganizationID",
                principalTable: "Organizations",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
