﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using EmployeeTable.Data;
using EmployeeTable.Models;

namespace EmployeeTable.Controllers
{
    public class EmployeePositionsController : Controller
    {
        private readonly EmployeeTableContext _context;

        public EmployeePositionsController(EmployeeTableContext context)
        {
            _context = context;
        }

        // GET: EmployeePositions
        public async Task<IActionResult> Index()
        {
            return View(await _context.EmployeePositions.ToListAsync());
        }

        
        // GET: EmployeePositions/Create
        public IActionResult Create()
        {
            return PartialView("_CreatePartial");
        }

        // POST: EmployeePositions/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Name")] EmployeePosition employeePosition)
        {
            if (EmployeePositionNameInUse(employeePosition.Name))
            {
                ModelState.AddModelError("Name", "Такая должность уже существует");
            }
            if (ModelState.IsValid)
            {
                _context.Add(employeePosition);
                await _context.SaveChangesAsync();
            }
            return PartialView("_CreatePartial", employeePosition);
        }
               

        // POST: EmployeePositions/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Name")] EmployeePosition employeePosition)
        {
            if (id != employeePosition.ID)
            {
                return NotFound();
            }
            if (EmployeePositionNameInUse(employeePosition.Name))
            {
                ModelState.AddModelError("Name", "Такая должность уже существует");
            }
            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(employeePosition);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!EmployeePositionExists(employeePosition.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(nameof(Index));
        }

        // POST: EmployeePositions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var employeePosition = await _context.EmployeePositions.FindAsync(id);
            _context.EmployeePositions.Remove(employeePosition);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool EmployeePositionExists(int id)
        {
            return _context.EmployeePositions.Any(e => e.ID == id);
        }

        private bool EmployeePositionNameInUse(string positionName)
        {
            return positionName != null && _context.EmployeePositions.Any(d => d.Name == positionName);
        }
    }
}
