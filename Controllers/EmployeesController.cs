﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using EmployeeTable.Data;
using EmployeeTable.Models;
using System.Diagnostics;

namespace EmployeeTable.Controllers
{
    public class EmployeesController : Controller
    {
        private readonly EmployeeTableContext _context;

        public EmployeesController(EmployeeTableContext context)
        {
            _context = context;
        }
		
		[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        // GET: Employees
        public async Task<IActionResult> Index()
        {
            var employeeTableContext = _context.Employees
                .Include(e => e.Workplace)
                    .ThenInclude(w => w.Department)
                .Include(e => e.Workplace)
                    .ThenInclude(w => w.Organization)
                .Include(e => e.Position);
            ViewData["Position"] = new SelectList(_context.EmployeePositions, "ID", "Name");
            ViewData["Organization"] = new SelectList(_context.Organizations, "ID", "Name");
            ViewData["Department"] = new SelectList(_context.Departments, "ID", "Name");
            return View(await employeeTableContext.ToListAsync());
        }

        // GET: Employees/Create
        public IActionResult Create()
        {
            ViewData["Position"] = new SelectList(_context.EmployeePositions, "ID", "Name");
            ViewData["Organization"] = new SelectList(_context.Organizations, "ID", "Name");
            ViewData["Department"] = new SelectList(_context.Departments, "ID", "Name");
            return View();
        }

        // POST: Employees/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Surname,Name,Otch,Phone,Email,PositionID, DepartmentID, OrganizationID")] EmployeeWorkplaceViewModel employeeViewModel)
        {
            if (ModelState.IsValid)
            {
                Workplace workplace = FindWorkplaceOrCreateNew(employeeViewModel.DepartmentID, employeeViewModel.OrganizationID);              
                _context.Add(employeeViewModel.GetEmployee(workplace.ID));
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["Position"] = new SelectList(_context.EmployeePositions, "ID", "Name", employeeViewModel.PositionID);
            ViewData["Organization"] = new SelectList(_context.Organizations, "ID", "Name", employeeViewModel.OrganizationID);
            ViewData["Department"] = new SelectList(_context.Departments, "ID", "Name", employeeViewModel.DepartmentID);

            return View(employeeViewModel);
        }

        // GET: Employees/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var employee = await _context.Employees
                .Include(e => e.Workplace)
                .SingleOrDefaultAsync(e => e.ID == id);
            if (employee == null)
            {
                return NotFound();
            }
            ViewData["Position"] = new SelectList(_context.EmployeePositions, "ID", "Name", employee.PositionID);
            ViewData["Organization"] = new SelectList(_context.Organizations, "ID", "Name", employee.Workplace.OrganizationID);
            ViewData["Department"] = new SelectList(_context.Departments, "ID", "Name", employee.Workplace.DepartmentID);

            return View(new EmployeeWorkplaceViewModel(employee));
        }

        // POST: Employees/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Surname,Name,Otch,Phone,Email,PositionID, DepartmentID, OrganizationID")] EmployeeWorkplaceViewModel employeeViewModel)
        {
            if (id != employeeViewModel.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                Workplace workplace = FindWorkplaceOrCreateNew(employeeViewModel.DepartmentID, employeeViewModel.OrganizationID);
                try
                {
                    _context.Update(employeeViewModel.GetEmployee(workplace.ID));
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!EmployeeExists(employeeViewModel.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["Position"] = new SelectList(_context.EmployeePositions, "ID", "Name", employeeViewModel.PositionID);
            ViewData["Organization"] = new SelectList(_context.Organizations, "ID", "Name", employeeViewModel.OrganizationID);
            ViewData["Department"] = new SelectList(_context.Departments, "ID", "Name", employeeViewModel.DepartmentID);
            return View(employeeViewModel);
        }

        
        // POST: Employees/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var employee = await _context.Employees.FindAsync(id);
            _context.Employees.Remove(employee);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool EmployeeExists(int id)
        {
            return _context.Employees.Any(e => e.ID == id);
        }

        private Workplace FindWorkplaceOrCreateNew(int departmentID, int organizationID)
        {
            Workplace workplace = _context.Workplace
                    .Where(w => w.DepartmentID == departmentID && w.OrganizationID == organizationID)
                    .SingleOrDefault();
            if (workplace == null)
            {
                workplace = new Workplace()
                {
                    OrganizationID = organizationID,
                    DepartmentID = departmentID
                };
                _context.Add(workplace);
                _context.SaveChanges();
            }
            return workplace;
        }
    }
}
