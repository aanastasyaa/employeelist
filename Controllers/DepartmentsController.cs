﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EmployeeTable.Data;
using EmployeeTable.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace EmployeeTable.Controllers
{
    public class DepartmentsController : Controller
    {
        private readonly EmployeeTableContext _context;

        public DepartmentsController(EmployeeTableContext context)
        {
            _context = context;
        }

        // GET: Departments
        public async Task<IActionResult> Index()
        {
            return View(await _context.Departments.ToListAsync());
        }


        // GET: Departments/Create
        public IActionResult Create()
        {
            return PartialView("_CreatePartial");
        }

        // POST: Departments/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Name")] Department department)
        {
            if(DepartmentNameInUse(department.Name))
            {
                ModelState.AddModelError("Name", "Департамент с таким названием уже существует");
            }
            if (ModelState.IsValid)
            {
                _context.Add(department);
                await _context.SaveChangesAsync();
            }
            return PartialView("_CreatePartial", department);
        }


        // POST: Departments/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Name")] Department department)
        {
            if (id != department.ID)
            {
                return NotFound();
            }
            if (DepartmentNameInUse(department.Name))
            {
                ModelState.AddModelError("Name", "Департамент с таким названием уже существует");
            }
            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(department);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DepartmentExists(department.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(nameof(Index));
        }

        // POST: Departments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var Department = await _context.Departments.FindAsync(id);
            _context.Departments.Remove(Department);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool DepartmentExists(int id)
        {
            return _context.Departments.Any(e => e.ID == id);
        }
        
        private bool DepartmentNameInUse(string departmentName)
        {
            return departmentName!=null && _context.Departments.Any(d => d.Name == departmentName);
        }
    }
}