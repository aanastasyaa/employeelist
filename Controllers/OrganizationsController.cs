﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EmployeeTable.Data;
using EmployeeTable.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace EmployeeTable.Controllers
{
    public class OrganizationsController : Controller
    {
        private readonly EmployeeTableContext _context;

        public OrganizationsController(EmployeeTableContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> Index()
        {
            return View(await _context.Organizations.ToListAsync());
        }

        // GET: Organizations/Create
        public IActionResult Create()
        {
            return PartialView("_CreatePartial");
        }

        // POST: Organizations/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Name")] Organization organization)
        {
            if(OrganizationNameInUse(organization.Name))
            {
                ModelState.AddModelError("Name", "Организация с таким именем уже существует");
            }
            if (ModelState.IsValid)
            {
                _context.Add(organization);
                await _context.SaveChangesAsync();
            }
            return PartialView("_CreatePartial", organization);
        }


        // POST: Organizations/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Name")] Organization organization)
        {
            if (id != organization.ID)
            {
                return NotFound();
            }
            if (OrganizationNameInUse(organization.Name))
            {
                ModelState.AddModelError("Name", "Организация с таким именем уже существует");
            }
            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(organization);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!OrganizationExists(organization.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(nameof(Index));
        }

        // POST: Organizations/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var organization = await _context.Organizations.FindAsync(id);
            _context.Organizations.Remove(organization);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool OrganizationExists(int id)
        {
            return _context.Organizations.Any(e => e.ID == id);
        }

        private bool OrganizationNameInUse(string organizationName)
        {
            return organizationName != null && _context.Organizations.Any(d => d.Name == organizationName);
        }
    }
}