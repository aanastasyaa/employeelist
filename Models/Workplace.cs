﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeTable.Models
{
    public class Workplace
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        public int DepartmentID { get; set; }

        public int OrganizationID { get; set; }

        public virtual Department Department { get; set; }
        public virtual Organization Organization { get; set; }
    }
}
