﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeTable.Models
{
    public class Employee
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Required(ErrorMessage = "Не указана фамилия сотрудника")]
        [Display(Name = "Фамилия")]
        public string Surname { get; set; }

        [Required(ErrorMessage = "Не указано имя сотрудника")]
        [Display(Name = "Имя")]
        public string Name { get; set; }

        [Display(Name = "Отчество")]
        public string Otch { get; set; }

        [Required(ErrorMessage = "Не указан телефон")]
        [Display(Name = "Телефон")]
        public string Phone { get; set; }

        [Required(ErrorMessage = "Не указан email")]
        [EmailAddress(ErrorMessage = "Недопустимый email: email должен иметь формат myemail@example.domain")]
        public string Email { get; set; }

        public int WorkplaceID { get; set; }   

        public virtual Workplace Workplace { get; set; }

        public int PositionID { get; set; }

        [Display(Name = "Должность")]
        public virtual EmployeePosition Position { get; set; }

        [Display(Name = "ФИО")]
        public string FullName
        {
            get
            {
                return Surname + " " + Name + " " + Otch;
            }
        }

        [Display(Name = "Организация")]
        public string Organization
        {
            get
            {
                return Workplace.Organization.Name;
            }
        }

        [Display(Name = "Департамент")]
        public string Department
        {
            get
            {
                return Workplace.Department.Name;
            }
        }
    }
}
