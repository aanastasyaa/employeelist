﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeTable.Models
{
    public class EmployeeWorkplaceViewModel
    {
        public int ID { get; set; }

        [Required(ErrorMessage = "Не указана фамилия сотрудника")]
        [Display(Name = "Фамилия")]
        public string Surname { get; set; }

        [Required(ErrorMessage = "Не указано имя сотрудника")]
        [Display(Name = "Имя")]
        public string Name { get; set; }

        [Display(Name = "Отчество")]
        public string Otch { get; set; }

        [Required(ErrorMessage = "Не указан телефон")]
        [Display(Name = "Телефон")]
        public string Phone { get; set; }

        [Required(ErrorMessage = "Не указан email")]
        [EmailAddress(ErrorMessage = "Недопустимый email: email должен иметь формат myemail@example.domain")]
        public string Email { get; set; }

        public int PositionID { get; set; }

        [Display(Name = "Должность")]
        public virtual EmployeePosition Position { get; set; }

        public int DepartmentID { get; set; }

        public int OrganizationID { get; set; }

        [Display(Name = "Департамент")]
        public virtual Department Department { get; set; }

        [Display(Name = "Организация")]
        public virtual Organization Organization { get; set; }

        public EmployeeWorkplaceViewModel(Employee employee)
        {
            ID = employee.ID;
            Surname = employee.Surname;
            Otch = employee.Otch;
            Name = employee.Name;
            Email = employee.Email;
            Phone = employee.Phone;
            PositionID = employee.PositionID;
            OrganizationID = employee.Workplace.OrganizationID;
            DepartmentID = employee.Workplace.DepartmentID;

        }

        public EmployeeWorkplaceViewModel()
        {

        }

        public Employee GetEmployee(int workplaceID)
        {
            return new Employee()
            {
                ID = ID,
                Surname = Surname,
                Name = Name,
                Otch = Otch,
                Phone = Phone,
                Email = Email,
                PositionID = PositionID,
                WorkplaceID = workplaceID
            };
        }


    }
}
