﻿using EmployeeTable.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeTable.Data
{
    public class EmployeeTableContext : DbContext
    {
        public EmployeeTableContext(DbContextOptions<EmployeeTableContext> options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Organization>().HasIndex(u => u.Name).IsUnique();
            modelBuilder.Entity<EmployeePosition>().HasIndex(u => u.Name).IsUnique();
            modelBuilder.Entity<Department>().HasIndex(u => u.Name ).IsUnique();
            modelBuilder.Entity<Workplace>().HasIndex(u => new { u.DepartmentID, u.OrganizationID }).IsUnique();

            
        }

        public DbSet<Employee> Employees { get; set; }
        public DbSet<Workplace> Workplace { get; set; }
        public DbSet<Organization> Organizations { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<EmployeePosition> EmployeePositions { get; set; }
    }

    
}
